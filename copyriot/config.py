from ruamel.yaml import YAML

class CopyriotConfig():
    def __init__(self, config_file: str = None, headers: str = None):
        self.valid = True
        self.headers = None
        self.ignore = None
        self.ignore_top_level = None

        if config_file is None:
            if headers is None:
                self.valid = False
            else:
                self.headers = None
        else:
            self.parse_file(config_file)

            # Override headers in config file if specified
            if headers is not None:
                self.headers = headers

            # Neither config or cli defined header
            if self.headers is None:
                self.valid = False

        return None

    def read_yaml_file(self, yaml_obj: YAML, config_file: str) -> dict:
        yaml_dict = None

        with open(config_file) as stream:
            yaml_dict = yaml_obj.load(stream)

        return yaml_dict
        
    def parse_file(self, config_file: str) -> None:
        yaml = YAML()

        try:
            yaml_dict = self.read_yaml_file(yaml, config_file)
        except OSError:
            print(f"Could not open config file: {config_file}")
            self.valid = False
            return
        except YAML.YAMLError as e:
            print(f"Could not parse config file:\n{e}")
            self.valid = False
            return

        try:
            self.headers = yaml_dict["header"]
        except KeyError:
            print("Warning: no header specified in config file")
            pass # Silently ignore if header not present

        try:
            self.ignore = yaml_dict["ignore"]
        except KeyError:
            pass # Silently ignore if ignore not present

        try:
            self.ignore_top_level = yaml_dict["ignore-top-level"]
        except KeyError:
            pass # Silently ignore if ignore not present

        return None

