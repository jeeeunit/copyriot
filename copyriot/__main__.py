# standard imports
import argparse
import sys

# copyriot imports
from .copyriot import add_header_to_file, process_directory
from .config import CopyriotConfig


def main():
    parser = argparse.ArgumentParser(description='Add headers to files in a directory.')
    parser.add_argument('--header', '-t', type=str, nargs='+', help='One or more header texts to add.')
    parser.add_argument('--config-file', '-c', type=str, help='The path to copyriot configuration file')
    content_source_group = parser.add_mutually_exclusive_group(required=True)
    content_source_group.add_argument('--file', '-f', type=str, nargs='+', help='The path to the file.')
    content_source_group.add_argument('--directory', '-d', type=str, help='The path to the directory.')
    args = parser.parse_args()

    copyriot_config_obj = CopyriotConfig(config_file=args.config_file)

    if not copyriot_config_obj.valid:
        print("Invalid copyriot configuration")
        parser.print_help()
        sys.exit(1)

    if args.file:
        for filepath in args.file:
            add_header_to_file(filepath, copyriot_config_obj.headers)
    elif args.directory:
        process_directory(args.directory, copyriot_config_obj)

if __name__ == "__main__":
    main()
