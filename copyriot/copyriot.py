import os
import re
from typing import Optional

from .constants import HEADERS, UTF_8
from .config import CopyriotConfig


def has_keywords(string: str, keywords: list[str]) -> bool:
    """Check whether string contains any of keywords."""
    return any(keyword.lower() in string.lower() for keyword in keywords)


def get_header_prefix(filename: str) -> Optional[str]:
    """Get header prefix for supported file."""
    header_prefix = None
    for ext, prefix in HEADERS.items():
        if filename.endswith(ext) or filename == ext:
            header_prefix = prefix
            break
    return header_prefix


def get_full_header(header_texts: str, header_prefix: str) -> str:
    """Get full header based on file-type."""
    if header_texts == '<!--':
        full_header = '\n'.join([f'{header_prefix} {header} -->' for header in header_texts])
    else:
        full_header = '\n'.join([f'{header_prefix} {header}' for header in header_texts])

    full_header += "\n"
    return full_header


def are_headers_present(filepath: str) -> bool:
    """Check whether headers already presented in the file."""
    with open(filepath, 'r+', encoding=UTF_8) as file:
        start_of_file = file.read(1024)

        if has_keywords(string=start_of_file, keywords=['license', 'copyright']):
            print(f"{filepath} skipped")
            return True
    return False


def update_file(filepath: str, full_header: str) -> None:
    """Update file with a header."""
    with open(filepath, 'r+', encoding=UTF_8) as file:
        original_content = file.read()

        # Determine if a shebang line is present
        lines = original_content.splitlines(True)
        if lines and lines[0].startswith('#!'):
            content = lines[0] + full_header + ''.join(lines[1:])
        else:
            content = full_header + original_content

        # Reset file pointer and overwrite the file
        file.seek(0)
        file.write(content)
        file.truncate()  # Ensure file is not longer than new content
        print(f"{filepath} is updated")
    return None


def add_header_to_file(filepath: str, header_texts: str) -> None:
    """Add header to the file. Skip if extension is not provided or header already exists."""
    filename = os.path.basename(filepath)
    header_prefix = get_header_prefix(filename=filename)

    if header_prefix is None:
        print(f"{filepath} does not have a recognized extension, skipped")
        return None  # Skip files without a matching extension

    full_header = get_full_header(header_texts=header_texts, header_prefix=header_prefix)

    if are_headers_present(filepath=filepath):
        return None

    update_file(filepath=filepath, full_header=full_header)
    return None


def check_against_ignore(root: str, top_level_dir: str, filename: str, copyriot_config_obj: CopyriotConfig) -> bool:
    """Check if file is included in configuration's ignore list"""
    ignore_file = False

    full_filename = os.path.join(root, filename)

    ignore_regex = rf"(?:{('|'.join(copyriot_config_obj.ignore))})"
    top_level_ignore_regex = rf"(?:{('|'.join(copyriot_config_obj.ignore_top_level))})"

    if root is top_level_dir:
        if re.match(top_level_ignore_regex, filename):
            print(f"Warning: Ignoring top-level file {full_filename}")
            ignore_file = True

    if re.match(ignore_regex, filename):
        print(f"Warning: Ignoring file {full_filename}")
        ignore_file = True
        
    return ignore_file


def process_directory(directory: str, copyriot_config_obj: CopyriotConfig) -> None:
    for root, _, files in os.walk(directory):
        for filename in files:

            ignore_file = check_against_ignore(root, directory, filename, copyriot_config_obj)

            if not ignore_file:
                filepath = os.path.join(root, filename)
                add_header_to_file(filepath, copyriot_config_obj.headers)
    return None
